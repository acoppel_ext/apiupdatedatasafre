<?php
	define('ARCHIVO_CONF_WS',			'wsconf.dat');
	define('path_wsdl', 				'wsHuellasHC.wsdl');
	define('DEFAULT__',					0);
	define('OK__',						1);
	define('ERR__',						-99);


	define('CONST_ESTATUS', 			1);
	define('CONST_CLASIFICACION_SERV', 	2);
	define('CONST_ENTIDAD_FEDERATIVA',	3);
	define('CONST_MEDIO_RECEPCION', 	4);
	define('CONST_SERVICIO', 			5);
	define('CONST_PRODUCTO', 			6);
	define('CONST_CAUSA', 				7);
	define('CONST_TIPO_TELEFONO', 		8);
	define('CONST_TIPO_RESOLUCION',		9);
	define('CONST_MOTIVO', 				10);

	function leerParametrosWs()
	{
		$sUrlWs = file_get_contents(ARCHIVO_CONF_WS);
		$sUrlWs = SUBSTR($sUrlWs, 0, - 1);
		return $sUrlWs;
	}
?>