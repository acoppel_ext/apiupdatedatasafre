<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//include Rest Controller library
require(APPPATH.'/libraries/REST_Controller.php');
define('RUTA_LOGX',					'/sysx/progs/afore/log/CtrlupdateDataSafre');

class CtrlupdateDataSafre extends REST_Controller{

    public function __construct() {

		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {die();}
        parent::__construct();
        ini_set('memory_limit','1024M');
        $this->load->model('MdlupdateDataSafre');
    }

    public function updateDataSafre_post()
    {
        $arrDatos = array();

        try {
            $response = $this->MdlUpdateDataSafre->updateDataSafre();

            if($response)
            {
                $arrDatos['estatus'] = 1;
                $arrDatos['descripcion'] = "Datos actualizados";
            }
        }catch (Exception $mensajeExcep){
            $this->grabarLogX("Error 500");
            $arrDatos['estatus'] = 0;
            $arrDatos['descripcion'] = "Error";
        }

        $this->response($arrDatos, REST_controller::HTTP_OK);
    }
    
    public function grabarLogX($cadLogx = "", $keyx = 0)
	{

		$rutaLog =  RUTA_LOGX .  '-' . date("Y-m-d") . ".log";
		$cad = date("Y-m|H:i:s|") . getmypid() . "| [" .  $keyx  . "] | " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}
}
?>